# 9 Letter Word Puzzle Solver

This is a Go lang implementation of the 9 letter word puzzle:

* [Nine Letter Word](http://nineletterword.tompaton.com/adevcrsoi/)
* [Your Word Life](http://www.yourwiselife.com.au/games/9-letter-word/)

![nineletterword.tompaton.com](nineletterword.png)

## Build

```bash
go build
```

## Example

To run program:

```bash
go run wordpuzzle.go -mandatory c -letters adevcrsoi
```

Or, once complied:

```bash
./wordpuzzle -mandatory c -letters adevcrsoi
```

Using this [dictionary](./dictionary) you should get 242 words.

    $ ./wordpuzzle -mandatory c -letters adevcrsoi | wc -l
    242

## Tests

To run tests:

```bash
go test -v
```

    $ go test -v
    === RUN   TestWordTooShort
    --- PASS: TestWordTooShort (0.00s)
    === RUN   TestWordTooLong
    --- PASS: TestWordTooLong (0.00s)
    === RUN   TestValidWord
    --- PASS: TestValidWord (0.00s)
    === RUN   TestNotValidWord
    --- PASS: TestNotValidWord (0.00s)
    === RUN   TestRemoveIndex
    --- PASS: TestRemoveIndex (0.00s)
    === RUN   TestLettersValid
    --- PASS: TestLettersValid (0.00s)
    === RUN   TestLettersInvalid
    --- PASS: TestLettersInvalid (0.00s)
    === RUN   TestLettersTooLong
    --- PASS: TestLettersTooLong (0.00s)
    === RUN   TestIsValidMandatoryGood
    --- PASS: TestIsValidMandatoryGood (0.00s)
    === RUN   TestIsValidyMandatoryBad
    --- PASS: TestIsValidyMandatoryBad (0.00s)
    === RUN   TestIsValidyMandatoryNotLetter
    --- PASS: TestIsValidyMandatoryNotLetter (0.00s)
    === RUN   TestIsValidyMandatoryEmpty
    --- PASS: TestIsValidyMandatoryEmpty (0.00s)
    PASS
    ok  	_/home/frank/dev/go/wordpuzzle	0.002s

## Other Implementations

* [Clojure](https://gitlab.com/frankhjung1/clojure-wordpuzzle)
* [Haskell](https://gitlab.com/frankhjung1/haskell-wordpuzzle)
* [Java](https://gitlab.com/frankhjung1/java-wordpuzzle)
* [Kotlin](https://gitlab.com/frankhjung1/kotlin-wordpuzzle)
* [Go](https://gitlab.com/frankhjung1/go-wordpuzzle)
* [Python](https://gitlab.com/frankhjung1/python-wordpuzzle)
