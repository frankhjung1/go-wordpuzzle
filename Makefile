#!/usr/bin/env make

ECHO	:= echo
SRCS	:= $(wildcard *.go **/*.go)
GOLINT	:= v1.50.0
TARGET	:= wordpuzzle

.DEFAULT_GOAL	:= test

.PHONY:	all build check clean cleanall deps help run test

all: check test build run

help:
	@$(ECHO)
	@$(ECHO) "Default goal: $(.DEFAULT_GOAL)"
	@$(ECHO) "  all:	check test "
	@$(ECHO) "  build:	build executable "
	@$(ECHO) "  check:	check style and lint code"
	@$(ECHO) "  clean:	delete generated files and directories"
	@$(ECHO) "  cleanall:	does a deep clean"
	@$(ECHO) "  deps:	install dependencies"
	@$(ECHO) "  run:   	run example"
	@$(ECHO) "  test:  	run unit tests"
	@$(ECHO)
	@$(ECHO) "First load dependencies using:"
	@$(ECHO)
	@$(ECHO) "  make deps"
	@$(ECHO)
	@$(ECHO) "Installed software:"
	@$(ECHO)
	@$(ECHO) "  $(shell go version)"
	@$(ECHO) "  $(shell golangci-lint --version)"
	@$(ECHO) "  $(shell go list -m)"
	@$(ECHO)
	@$(ECHO) "Get latest version on golangci-lint from:"
	@$(ECHO) "  https://github.com/golangci/golangci-lint/tags"

check:
	# check code formatted ...
	@test -z $(gofmt -l $(SRCS))
	# vet code ...
	@go vet $(SRCS)
	# lint code ...
	@golangci-lint run $(SRCS)
	# verify packages ...
	@go mod verify

test:	build
	@go test -cover -v $(SRCS)

build:	check
	@go build -v -o $(TARGET)

run:
	-$(CURDIR)/$(TARGET) -mandatory c -size 7 -letters adevcrsoi

deps:
	# initialise package
	@go mod init gitlab.com/frankhjung1/go-wordpuzzle
	# binary will be $(go env GOPATH)/bin/golangci-lint
	@curl -sSfL https://raw.githubusercontent.com/golangci/golangci-lint/master/install.sh | sh -s -- -b $(shell go env GOPATH)/bin $(GOLINT)
	# get dependencies
	@go get -d -v $(CURDIR)
	# tidy package dependencies
	@go mod tidy

clean:
	@go clean -x
	@rm -f $(wildcat *.out **/*.out)


cleanall: clean
	@rm -f $(TARGET)
	@rm -f go.mod
	@#rm -f $(shell go env GOPATH)/bin/golangci-lint
